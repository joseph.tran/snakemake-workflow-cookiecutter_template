# snakemake-workflow-cookiecutter_template
![snakemake](img/snakemake.png)

Cookiecutter template for snakemake workflows.

---

## Documentation  
  
* [gitbook: snakemake workflow cookiecutter template](https://joseph.tran.pages.mia.inra.fr/snakemake-workflow-cookiecutter_template)

## Install cookiecutter  
  
* [readthedocs: cookiecutter](https://cookiecutter.readthedocs.io/en/latest/README.html)  
* [install cookiecutter](https://cookiecutter.readthedocs.io/en/latest/installation.html)  
  
## Grab snakemake workflow template  
  
Once you have successfully installed cookiecutter, you are ready to grab the cookiecutter template.

```bash
cookiecutter git@forgemia.inra.fr:joseph.tran/snakemake-workflow-cookiecutter_template.git
# or
cookiecutter https://forgemia.inra.fr/joseph.tran/snakemake-workflow-cookiecutter_template.git 

```
  
Then you will be prompted to configure the new snakemake workflow project.  
You need to fill the different keys with your own information values.
Here are the default values, if you skip setting any value when prompted:   
  
```json
{
  "full_name": "Joseph Tran",
  "email": "joseph.tran@inrae.fr",
  "username": "joseph.tran",
  "project_name": "Snakemake-workflow",
  "repo_name": "smk-workflow",
  "min_snakemake_version": "5.7.0"
}
```

When you are done with this configuration step, you have a fresh new snakemake workflow project, with the project name you filled along with other information to setup your project.  

Your are almost done starting your new snakemake workflow development. There are still some post-processing, optional but recommended.   

## Post-process  
  
### Link to a Git repository 

You need first to create a project on github, gitlab or anywhere you want to push your code to a central repository.
In this following example, I chose to push to gitlab hosted on https://forgemia.inra.fr.  

1. connect to https://forgemia.inra.fr/ using your institutional credentials (INRAE for instance)  
2. create a new project (using avaliable template if needed)  
3. copy the clone URL, git@... or htpps:// 
4. return to the terminal to your snakemake workflow project, and issue the following commands to setup the gitlab link:  

```bash
cd my_snakemake_workflow_project
git init
git add .
git commit -m "template version"
# paste the git URL following the git remote command
git remote add origin git@forgemia.inra.fr:joseph.tran/my_snakemake_workflow.git
git push -u origin --all
git push -u origin --tags

``` 
  
You now had setup your project to the central gitalb repository.  
  
### SLURM profile  
  
If you intend to work on HPC using slurm for instance, you will need to setup a slurm profile.  
Since snakemake version 5.10, cluster configuration was deprecated to the benefit of snakemake cluster profile.
Slurm profile can be installed using cookiecutter also.  

Follow the instructions on [snakemake slurm profile](https://github.com/Snakemake-Profiles/slurm) page to install slurm profile.

To setup resources to your rule, as memory or time, use **resource** directive as described [here](https://snakemake.readthedocs.io/en/stable/snakefiles/rules.html#snakefiles-resources).  
Here is a list of resource keywords mapping to slurm resource keyword: "slurm resource keyword": ("snakemake resource keywords list")  

```python
# slurm-submit.RESOURCE_MAPPING
RESOURCE_MAPPING = {
    "time": ("time", "runtime", "walltime"),
    "mem": ("mem", "mem_mb", "ram", "memory"),
    "mem-per-cpu": ("mem-per-cpu", "mem_per_cpu", "mem_per_thread"),
    "nodes": ("nodes", "nnodes"),
}

```

The number of cpus is controlled by the **threads** directive.  
  
It is not recommended anymore to use cluster configuration file, although it is still possible to provide one, at slurm profile setup time or on snakemake command line with ##--cluster-config## option.  
  
Here is an example to create a new snakemake slurm profile for genotoul cluster:  
  
```bash
# example: genotoul slurm cluster with default workq partition
$ cd config
$ cookiecutter https://github.com/Snakemake-Profiles/slurm.git
profile_name [slurm]: slurm.genotoul
sbatch_defaults []: job-name={rule} partition=workq mem=2000M error=results/slurm_logs/{rule}.%N.%j.err output=results/slurm_logs/{rule}.%N.%j.out
cluster_config []: 
Select advanced_argument_conversion:
1 - no
2 - yes
Choose from 1, 2 [1]: 
cluster_name []: genobull

```

