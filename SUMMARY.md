# Summary

* [Introduction](README.md)
    * [Install cookiecutter](README.md#install-cookiecutter)  
    * [Grab snakemake workflow template](README.md#grab-snakemake-workflow-template)
    * [Post-process](README.md#post-process)
* [Gitbook management](gitbook.md)
