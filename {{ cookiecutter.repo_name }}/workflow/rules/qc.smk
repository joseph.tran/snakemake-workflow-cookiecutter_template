rule fastqc:
    input:
        unpack(get_fastq_end)
    output:
        html= RESDIR+"/qc/fastqc/raw/{sample}-{unit}_{end}_fastqc.html",
        zip=RESDIR+"/qc/fastqc/raw/{sample}-{unit}_{end}_fastqc.zip"
    log:
        RESDIR+"/logs/fastqc_raw_{sample}-{unit}_{end}.log"
    resources:
        mem_mb=4000
    threads: 1
    wrapper:
        "0.72.0/bio/fastqc" 


rule multiqc:
    input:
        # single read
        expand([RESDIR+"/qc/fastqc/raw/{u.sample}-{u.unit}_{end}_fastqc.zip"],
                u=units_se.itertuples(), end=["r1"]) if not units_se.empty else "{}".format("config/config.yaml"),
        # paired-end
        expand([RESDIR+"/qc/fastqc/raw/{u.sample}-{u.unit}_{end}_fastqc.zip"], u=units_pe.itertuples(), end=["r1", "r2"]) if not units_pe.empty else "{}".format("config/config.yaml")
    output:
        report("{}/qc/multiqc.html".format(RESDIR), caption="../report/multiqc.rst", category="Quality control")
    log:
        "{}/logs/multiqc.log".format(RESDIR)
    resources:
        mem_mb=2000
    threads: 2
    wrapper:
        "0.72.0/bio/multiqc"


