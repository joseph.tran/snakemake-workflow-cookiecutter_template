import math
import os
import pandas as pd
import tempfile
from snakemake.shell import shell
from snakemake.utils import validate, min_version

#### set minimum snakemake version ####
min_version("5.4.7") 

#### requirement: load config #### 

configfile: "config/config.yaml"
validate(config, schema="../schemas/config.schema.yaml") 

#### load sample sheets #### 

def strip_columns(df):
    """
        Remove all leading and trailing spaces for each string value accross all series in dataframe
    """
    strip_strings = lambda x: x.strip() if isinstance(x, str) else x
    return df.applymap(strip_strings)

samples = pd.read_csv(config["samples"], sep="\t")
samples = strip_columns(samples)
samples = samples.set_index("sample", drop=False)
validate(samples, schema="../schemas/samples.schema.yaml") 

units_unsorted = pd.read_csv(config["units"], dtype=str, sep="\t")
units_unsorted = strip_columns(units_unsorted)
units_unsorted = units_unsorted.set_index(["sample", "unit"], drop=False)
units_unsorted.index = units_unsorted.index.set_levels([i.astype(str) for i in units_unsorted.index.levels])  # enforce str in index
units=units_unsorted.sort_index()
validate(units, schema="../schemas/units.schema.yaml") 

is_pe=pd.notna(units["r2"])

## single read units
units_se=units[~is_pe]

## paired-end units
units_pe=units[is_pe]


#### Wildcard constraints ####
wildcard_constraints:
    sample="|".join(samples.index),
    unit="|".join(units["unit"])

#### Helper functions ####

def get_fastq(wildcards):
    """Get fastq files of given sample-unit."""
    fastqs = units.loc[(wildcards.sample, wildcards.unit), ["r1", "r2"]].dropna()
    if len(fastqs) == 2:
        return {"r1": fastqs.r1, "r2": fastqs.r2}
    return {"r1": fastqs.r1}


def get_fastq_end(wildcards):
    """Get fastq files forward or reverse of given sample-unit-end."""
    fastqs = units.loc[(wildcards.sample, wildcards.unit), ].dropna()
    return {"end": fastqs[wildcards.end]}


def is_single_end(sample, unit):
        """Return True if sample-unit is single end."""
        return math.isnan(units.loc[(sample, unit), "r2"])


