rule backup_config:
    """
       backup all config files
    """
    params:
        outdir="{}/config".format(RESDIR)
    input:
        "config/config.yaml",
        "config/samples.tsv",
        "config/units.tsv"
    output:
        report("{}/config/config.yaml".format(RESDIR), caption="../report/config.rst", category="Config"),
        report("{}/config/samples.tsv".format(RESDIR), caption="../report/samples.rst", category="Config"),
        report("{}/config/units.tsv".format(RESDIR), caption="../report/units.rst", category="Config")
    shell: """
        cp {input} {params.outdir}/
        touch {params.outdir}/config.bkp.done
    """


