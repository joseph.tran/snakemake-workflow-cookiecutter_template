Units input file.

.. csv-table:: Units
    :file: {{ '{{' }} snakemake.config["resdir"] {{ '}}' }}/config/units.tsv
    :delim: 0x09
    :header-rows: 1
    :align: right
