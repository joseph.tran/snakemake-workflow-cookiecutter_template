Samples input file.

.. csv-table:: Samples
    :file: {{ '{{' }} snakemake.config["resdir"] {{ '}}' }}/config/samples.tsv
    :delim: 0x09
    :header-rows: 1
    :align: right
